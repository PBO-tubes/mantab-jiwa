-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2017 at 06:22 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventaris`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftarbaranggudang`
--

CREATE TABLE `daftarbaranggudang` (
  `no` int(11) NOT NULL,
  `id` varchar(10) NOT NULL,
  `namaBarang` varchar(25) NOT NULL,
  `jenisBarang` varchar(15) NOT NULL,
  `stock` int(11) NOT NULL,
  `id_penyedia` int(11) NOT NULL,
  `id_gudang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daftarbarangpenyedia`
--

CREATE TABLE `daftarbarangpenyedia` (
  `idBarang` varchar(10) NOT NULL,
  `namaBarang` varchar(25) NOT NULL,
  `jenisBarang` varchar(15) NOT NULL,
  `stock` int(11) NOT NULL,
  `id_penyedia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftarbarangpenyedia`
--

INSERT INTO `daftarbarangpenyedia` (`idBarang`, `namaBarang`, `jenisBarang`, `stock`, `id_penyedia`) VALUES
('INV1BIO', 'biola', 'alat musik', 2, 1),
('INV4BUK', 'buku', 'buku', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `daftargudang`
--

CREATE TABLE `daftargudang` (
  `id_gudang` varchar(10) NOT NULL,
  `slot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftargudang`
--

INSERT INTO `daftargudang` (`id_gudang`, `slot`) VALUES
('biola', 20),
('sabun', 15);

-- --------------------------------------------------------

--
-- Table structure for table `daftarorang`
--

CREATE TABLE `daftarorang` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `job` int(11) NOT NULL COMMENT '1=petugas, 2=penyedia, 3=admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftarorang`
--

INSERT INTO `daftarorang` (`id`, `nama`, `username`, `password`, `job`) VALUES
(0, 'Administrator', 'admin', 'password', 3),
(1, 'budi', 'budi', '12345', 2),
(3, 'Angga ganteng', 'anggaganteng', 'anggaganteng', 2),
(4, 'lala', 'lala', '12345', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftarbaranggudang`
--
ALTER TABLE `daftarbaranggudang`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `daftarbarangpenyedia`
--
ALTER TABLE `daftarbarangpenyedia`
  ADD PRIMARY KEY (`idBarang`);

--
-- Indexes for table `daftargudang`
--
ALTER TABLE `daftargudang`
  ADD PRIMARY KEY (`id_gudang`);

--
-- Indexes for table `daftarorang`
--
ALTER TABLE `daftarorang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftarbaranggudang`
--
ALTER TABLE `daftarbaranggudang`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
